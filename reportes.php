<?php
require 'classes/clsConnectMySql.php';
require 'classes/clsReportes.php';
require 'classes/clsProducto.php';

$page="reportes";

$db = new DB();
$reportes = new Reportes($db);
$productos = new Productos($db);

$tipos = $productos->getTipos();

$ayer7_ = mktime(0,0,0,date("m"),date("d")-7,date("Y"));
$ayer7 = date("Y-m-d", $ayer7_);
$ayer6_ = mktime(0,0,0,date("m"),date("d")-6,date("Y"));
$ayer6 = date("Y-m-d", $ayer6_);
$ayer5_ = mktime(0,0,0,date("m"),date("d")-5,date("Y"));
$ayer5 = date("Y-m-d", $ayer5_);
$ayer4_ = mktime(0,0,0,date("m"),date("d")-4,date("Y"));
$ayer4 = date("Y-m-d", $ayer4_);
$ayer3_ = mktime(0,0,0,date("m"),date("d")-3,date("Y"));
$ayer3 = date("Y-m-d", $ayer3_);
$ayer2_ = mktime(0,0,0,date("m"),date("d")-2,date("Y"));
$ayer2 = date("Y-m-d", $ayer2_);
$ayer_ = mktime(0,0,0,date("m"),date("d")-1,date("Y"));
$ayer = date("Y-m-d", $ayer_);
$hoy_ = mktime(0,0,0,date("m"),date("d"),date("Y"));
$hoy = date("Y-m-d");

$monto_ayer7 = round($reportes->monto_rango_dias($ayer7,$ayer7),2);
$monto_ayer6 = round($reportes->monto_rango_dias($ayer6,$ayer6),2);
$monto_ayer5 = round($reportes->monto_rango_dias($ayer5,$ayer5),2);
$monto_ayer4 = round($reportes->monto_rango_dias($ayer4,$ayer4),2);
$monto_ayer3 = round($reportes->monto_rango_dias($ayer3,$ayer3),2);
$monto_ayer2 = round($reportes->monto_rango_dias($ayer2,$ayer2),2);
$monto_ayer = round($reportes->monto_rango_dias($ayer,$ayer),2);
$monto_hoy = round($reportes->monto_rango_dias($hoy,$hoy),2);

?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Administraci&oacute;n - Reportes</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link href="css/font-awesome.css" rel="stylesheet">

    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    <?php include('includes/topmenu.php'); ?>

    <div class="content row">

      <div class="row">
        <div class="large-4 columns">
          <h3>Total de ventas para los &uacute;ltimos 7 dias</h3>
          <p>
          Monto total de <?php echo date("d/m", $ayer7_); ?>: $ <?php echo $monto_ayer7; ?><br/>
          Monto total de <?php echo date("d/m", $ayer6_); ?>: $ <?php echo $monto_ayer6; ?><br/>
          Monto total de <?php echo date("d/m", $ayer5_); ?>: $ <?php echo $monto_ayer5; ?><br/>
          Monto total de <?php echo date("d/m", $ayer4_); ?>: $ <?php echo $monto_ayer4; ?><br/>
          Monto total de <?php echo date("d/m", $ayer3_); ?>: $ <?php echo $monto_ayer3; ?><br/>
          Monto total de <?php echo date("d/m", $ayer2_); ?>: $ <?php echo $monto_ayer2; ?><br/>
          Monto total de ayer (<?php echo date("d/m", $ayer_); ?>): $ <?php echo $monto_ayer; ?><br/>
          Monto total de hoy (<?php echo date("d/m", $hoy_); ?>): $ <?php echo $monto_hoy; ?>
          </p>
        </div>
        <div class="large-8 columns">
          <div id="graph01">graph</div>
        </div>
      </div>

      <div class="row">
        <div class="large-12 columns">
          <div class="panel">
            <h3>$ Total de ventas - Desde - Hasta - Tipo</h3>
            <table class="table fullwidth">
              <thead>
                <tr>
                  <th>Desde&nbsp;
                    <a href="#" class="button small" id="dp4" data-date-format="yyyy-mm-dd" data-date="<?php echo $hoy; ?>">Cambiar</a>
                  </th>
                  <th>Hasta&nbsp;
                    <a href="#" class="button small" id="dp5" data-date-format="yyyy-mm-dd" data-date="<?php echo $hoy; ?>">Cambiar</a>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td id="startDate"><?php echo $hoy; ?></td>
                  <td id="endDate"><?php echo $hoy; ?></td>
                </tr>
              </tbody>
            </table>
            <div class="alert alert-box" id="alert"><strong></strong></div>
            <label>Tipo
              <select name="tipo" id="tipo">
                <option value="0">Elegir Tipo</option>
                <?php while($row = mysql_fetch_assoc($tipos)) { ?>
                  <option value="<?=$row['tipo'] ?>"><?=$row['tipo'] ?> (<?=$row['c'] ?>)</option>
                <?php } ?>
              </select>
            </label>
            <a href="#" id="calculartotal" class="button radius">Calcular total</a>
            <div class="totalreport">&nbsp;</div>
          </div>
        </div>
      </div>
    </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/highcharts.js"></script>
    <script src="js/vendor/exporting.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/foundation/foundation.datepicker.js"></script>
    <script>
      $(document).foundation();

      $(document).ready(function(){
        $('#graph01').highcharts({
            chart: {
                type: 'column',
                margin: [ 50, 50, 100, 80]
            },
            title: {
                text: 'Total de ventas par los últimos 7 dias'
            },
            xAxis: {
                categories: [
                    '<?php echo date("d/m", $ayer7_); ?>',
                    '<?php echo date("d/m", $ayer6_); ?>',
                    '<?php echo date("d/m", $ayer5_); ?>',
                    '<?php echo date("d/m", $ayer4_); ?>',
                    '<?php echo date("d/m", $ayer3_); ?>',
                    '<?php echo date("d/m", $ayer2_); ?>',
                    '<?php echo date("d/m", $ayer_); ?>',
                    '<?php echo date("d/m", $hoy_); ?>'
                ],
                labels: {
                    rotation: -45,
                    align: 'right',
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total de ventas'
                }
            },
            legend: {
                enabled: false
            },
            credits: {
              enabled: false
            },
            tooltip: {
                pointFormat: 'Total: <b>$ {point.y:.1f}</b>',
            },
            series: [{
                name: 'Total',
                data: [
                  <?php echo $monto_ayer7; ?>,
                  <?php echo $monto_ayer6; ?>,
                  <?php echo $monto_ayer5; ?>,
                  <?php echo $monto_ayer4; ?>,
                  <?php echo $monto_ayer3; ?>,
                  <?php echo $monto_ayer2; ?>,
                  <?php echo $monto_ayer; ?>,
                  <?php echo $monto_hoy; ?>
                  ],
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    x: 4,
                    y: 10,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif',
                        textShadow: '0 0 3px black'
                    }
                }
            }]
        });

        $('#alert').hide();
        
        var startDate = new Date();
        var endDate = new Date();

        $('#dp1').fdatepicker();
        $('#dp4').fdatepicker()
          .on('changeDate', function (ev) {
          if (ev.date.valueOf() > endDate.valueOf()) {
            $('#alert').show().find('strong').text('La fecha DESDE no puede ser despues de la fecha HASTA');
          } else {
            $('#alert').hide();
            startDate = new Date(ev.date);
            $('#startDate').text($('#dp4').data('date'));
          }
          $('#dp4').fdatepicker('hide');
        });

        $('#dp5').fdatepicker()
          .on('changeDate', function (ev) {
          if (ev.date.valueOf() < startDate.valueOf()) {
            $('#alert').show().find('strong').text('La fecha HASTA no puede ser antes de la fecha DESDE');
          } else {
            $('#alert').hide();
            endDate = new Date(ev.date);
            $('#endDate').text($('#dp5').data('date'));
          }
          $('#dp5').fdatepicker('hide');
        });
    });
    </script>
    <script src="js/reportes.js"></script>
  </body>
</html>

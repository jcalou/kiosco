<?php
$page="home";
require 'classes/clsConnectMySql.php';
$db = new DB();
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Administraci&oacute;n</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/style.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    <?php include('includes/topmenu.php'); ?>

    <div class="content row panel">
      <div class="large-4 columns">
        <a href="venta.php" class="button radius nomargin">Ingresar nueva Venta</a>
      </div>
      <div class="large-4 columns">
        <a href="egreso.php" class="button radius nomargin">Egreso de Dinero</a>
      </div>
      <div class="large-4 columns">
        <a href="caja.php" class="button radius nomargin">Caja</a>
      </div>
    </div>
    <div class="content row panel">
      <div class="large-12 columns">
        <h4>Productos</h4>
      </div>
      <div class="large-6 columns">
        <a href="listar-productos.php" class="button radius nomargin">Listar todos los Productos</a>
      </div>
      <div class="large-6 columns">
        <a href="producto.php" class="button radius nomargin">Agregar Nuevo Producto</a>
      </div>
    </div>
    <div class="content row panel">
      <div class="large-12 columns">
        <h4>Proveedores</h4>
      </div>
      <div class="large-6 columns">
        <a href="listar-proveedores.php" class="button radius nomargin">Listar todos los Proveedores</a>
      </div>
      <div class="large-6 columns">
        <a href="proveedor.php" class="button radius nomargin">Agregar Nuevo Proveedor</a>
      </div>
    </div>
    <div class="content row panel">
      <div class="large-6 columns">
        <h4>Reportes</h4>
      </div>
      <div class="large-6 columns">
        <h4>Backup</h4>
      </div>
      <div class="large-6 columns">
        <a href="reportes.php" class="button radius nomargin">Ver Reportes</a>
      </div>
      <div class="large-6 columns">
        <a href="backup.php" class="button radius nomargin">Realizar Backup</a>
      </div>
    </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>

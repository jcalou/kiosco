<?php
require 'classes/clsConnectMySql.php';
require 'classes/clsVenta.php';
require 'classes/clsCaja.php';

$db = new DB();
$ventas = new Ventas($db);
$caja = new Caja($db);

if (isset($_GET['e']) && ($_GET['e'] == "1")) {
  //eliminar
  $id = $_GET['id'];
  $ventas->eliminarEgreso($id);
  header("Location: egreso.php");
  die();
}

$ventas->observaciones = $_POST['descripcion'];
$ventas->total = $_POST['monto']*(-1);

$ventas->agregarEgreso();

$cajavalue = mysql_fetch_assoc($caja->getLast());

$caja->descripcion = "Egreso: ".$_POST['descripcion'];
$caja->monto = $_POST['monto']*(-1);
$caja->caja = str_replace(".",",",(string) (Getfloat($cajavalue['caja']) - Getfloat($_POST['monto'])));

$caja->agregar();

header("Location: egreso.php");
die();
?>

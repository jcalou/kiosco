<?php
require 'classes/clsConnectMySql.php';
require 'classes/clsProveedor.php';
$page="proveedores";

$db = new DB();
$proveedores = new Proveedores($db);

if(isset($_GET['id'])){
  $proveedor = mysql_fetch_array($proveedores->getId($_GET['id']));
  $tipo = "editar";
}else{
  $proveedor['id_proveedor'] = "0";
  $tipo = "agregar";
}

?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Administracion - Detalle de proveedor</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/style.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    <?php include('includes/topmenu.php'); ?>

    <div class=" content row">
      <div class="large-12 columns">
        <form action="procesar-proveedor.php" method="POST">
          <?php  if($tipo == "editar") { ?>
          <input type="hidden" name="id" value='<?php echo $proveedor['id']; ?>'>
          <?php }else{ ?>
          <input type="hidden" name="id" value="0">
          <?php } ?>
          <div class="row">
            <div class="large-6 columns">
              <label>Nombre
                <input type="text" name="nombre" value='<?=($tipo == "editar")?$proveedor['nombre']:""?>' required />
              </label>
            </div>
            <div class="large-6 columns">
              <label>Direcci&oacute;n
                <input type="text" name="direccion" value='<?=($tipo == "editar")?$proveedor['direccion']:""?>' />
              </label>
            </div>
          </div>
          <div class="row">
            <div class="large-6 columns">
              <label>Tel&eacute;fono
                <input type="text" name="telefono" value='<?=($tipo == "editar")?$proveedor['telefono']:""?>' />
              </label>
            </div>
            <div class="large-6 columns">
              <label>Contacto
                <input type="text" name="contacto" value='<?=($tipo == "editar")?$proveedor['contacto']:""?>' />
              </label>
            </div>
          </div>
          <div class="row">
            <div class="large-6 columns">
              <label>E-mail
                <input type="email" name="email" value='<?=($tipo == "editar")?$proveedor['email']:""?>' email/>
              </label>
            </div>
            <div class="large-6 columns">
              <label>CUIT
                <input type="text" name="cuit" value='<?=($tipo == "editar")?$proveedor['cuit']:""?>' />
              </label>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <label>Observaciones
                <textarea name="observaciones" id="observaciones"><?=($tipo == "editar")?$proveedor['observaciones']:""?></textarea>
              </label>
            </div>
          </div>
          <div class="row">
            <div class="large-4 columns">
              <input type="submit" value='Guardar' class="button success" />
              <a href="javascript:history.back();" class="button secondary">Cancelar</a>
            </div>
          </div>

        </form>
      </div>
    </div>


    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>

  </body>
</html>


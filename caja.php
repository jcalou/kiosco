<?php
$page="caja";

require 'classes/clsConnectMySql.php';

$db = new DB();

?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Administracion - Caja</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/themes/base/jquery.ui.all.css">
    <link rel="stylesheet" href="css/style.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    <?php include('includes/topmenu.php'); ?>
    <?php $result = $caja->getAll(); ?>
    <div class="content row">
      <div class="large-12 columns">
        <h4>Actualizaci&oacute;n de caja</h4>
      </div>
      <form action="procesar-caja.php" method="POST">
        <div class="large-6 columns">
          <div class="row collapse">
            <label>Descripcion
              <input type="text" placeholder="Descripcion" name="descripcion" id="descripcion" required />
            </label>
          </div>
        </div>
        <div class="large-4 columns">
          <div class="row collapse">
            <label>Dinero en la caja
              <input type="text" placeholder="En $" name="monto" id="monto" required />
            </label>
          </div>
        </div>

        <input type="hidden" id="id_hidden" value="0" />

        <div class="large-2 columns">
          <label>&nbsp;
            <input type="submit" value="Actualizar" class="button postfix" />
          </label>
        </div>

      </form>
    </div>

    <div class=" content row">
      <div class="large-12 columns">
        <h4>Listado de &uacute;ltimos movimientos de caja</h4>
      </div>
      <div class="large-12 columns">
        <table width="100%">
          <thead>
            <tr>
              <th>Fecha</th>
              <th>Descripcion</th>
              <th>Monto</th>
              <th>Caja</th>
            </tr>
          </thead>
          <tbody>
          <?php while($row = mysql_fetch_assoc($result)) { ?>
            <tr>
              <td><?=date("d/m/Y",strtotime($row['fecha_carga'])) ?></td>
              <td><?=$row['descripcion'] ?></td>
              <td>$ <?=$row['monto'] ?></td>
              <td class="caja">$ <?=$row['caja'] ?></td>
            </tr>
          <?php }; ?>
          </tbody>
        </table>
      </div>

    </div>
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>

    <script>
      $(document).foundation();
    </script>
  </body>
</html>

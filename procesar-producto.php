<?php
require 'classes/clsConnectMySql.php';
require 'classes/clsProducto.php';

$db = new DB();
$productos = new Productos($db);

if (isset($_GET['e']) && ($_GET['e'] == "1")) {
  //eliminar
  $id = $_GET['id'];
  $productos->eliminar($id);
  header("Location: listar-productos.php");
  die();
}

$productos->nombre = $_POST['nombre'];
$productos->tipo = $_POST['tipo'];
$productos->id_proveedor = $_POST['id_proveedor'];
$productos->codigo_de_barra = $_POST['codigo_de_barra'];
$productos->marca = $_POST['marca'];
$productos->descripcion = $_POST['descripcion'];
if ($_POST['stock']!="") {
  $productos->stock = $_POST['stock'];
} else {
  $productos->stock = "0";
}
if ($_POST['stock_minimo']!="") {
  $productos->stock_minimo = $_POST['stock_minimo'];
} else {
  $productos->stock_minimo = "0";
}
$productos->costo = $_POST['costo'];
$productos->margen = $_POST['margen'];
$productos->precio = $_POST['precio'];

if ($_POST['id']=="0") {
  // agregar
  $productos->agregar();
}else {
  // editar
  $productos->actualizar($_POST['id']);
};

header("Location: listar-productos.php");
die();
?>

<?php
require 'classes/clsConnectMySql.php';
require 'classes/clsVenta.php';
require 'classes/clsCaja.php';

$db = new DB();
$ventas = new Ventas($db);
$caja = new Caja($db);

$ventas->observaciones = $_POST['observaciones'];
$ventas->total = $_POST['total'];
if ($_POST['pago_con'] != ''){
  $ventas->pago_con = $_POST['pago_con'];
}else{
  $ventas->pago_con = 0;
}
$ventas->vuelto = $_POST['vuelto'];
if($_POST['cuenta_corriente']=="true") {
  $ventas->cuenta_corriente = "S";
}else{
  $ventas->cuenta_corriente = "N";
}

$id = $ventas->agregar();

// las ventas de cuenta corriente no generan movimiento de caja
if($_POST['cuenta_corriente'] != "true") {
  $cajavalue = mysql_fetch_assoc($caja->getLast());

  $caja->descripcion = "Venta id: ".$id;
  $caja->monto = $_POST['total'];
  $caja->caja = str_replace(".",",",(string) (Getfloat($cajavalue['caja']) + Getfloat($_POST['total'])));

  $caja->agregar();
}

echo $id;
exit;
?>

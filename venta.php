<?php $page="venta";
require 'classes/clsConnectMySql.php';
require 'classes/clsProducto.php';

$db = new DB();
$productos = new Productos($db);

$producto = $productos->getAll("",0,0);

?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Administracion</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/themes/base/jquery.ui.all.css">
    <link rel="stylesheet" href="css/style.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    <?php include('includes/topmenu.php'); ?>

    <div class="content row">
      <div class="large-1 columns">
        <div class="row collapse">
          <label>Cantidad
            <select name="" id="cantidad">
              <option value="1" selected>1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="8">8</option>
              <option value="9">9</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
              <option value="19">19</option>
              <option value="20">20</option>
            </select>
          </label>
        </div>
        <div class="row collapse">
          <img src="img/barcode-icon.png" alt="" id="bc-image">
        </div>
      </div>
      <div class="large-4 columns">
        <div class="row collapse">
          <label>Producto
            <input type="text" placeholder="Nombre" name="q" id="q" required />
          </label>
        </div>
        <div class="row collapse">
          <label>Codigo de Barras
            <input type="text" placeholder="Codigo de producto" name="qbc" id="qbc" required />
            <input type="hidden" name="qbc-name" id="qbc-name" />
          </label>
        </div>
      </div>
      <div class="large-2 columns">
        <label>Precio Unitario
          <input type="text" id="preciounitario" />
        </label>
      </div>
      <div class="large-2 columns">
        <label>Total
          <input type="text" id="preciototal" disabled />
        </label>
      </div>

      <input type="hidden" id="id_hidden" value="0" />

      <div class="large-2 columns">
        <label>&nbsp;
          <a href="#" class="button postfix" id="agregaritem">Agregar</a>
        </label>
      </div>
      <div class="large-1 columns">
        <label>&nbsp;
          <a href="" class="button postfix" id="limpiaritem">Limpiar</a>
        </label>
      </div>
    </div>

    <div class="content row">
        <div class="large-8 columns">
          <table width="100%" id="items">
            <thead>
              <tr>
                <th>Producto</th>
                <th width="55">$ x U</th>
                <th width="55">Cant.</th>
                <th width="55">$</th>
                <th width="30"></th>
                <th class="item_id"></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
          <label>Observaciones / Comentarios
            <textarea name="observaciones" id="observaciones"></textarea>
          </label>
        </div>
        <div class="large-4 columns">
          <fieldset class="total">
            <legend>TOTAL</legend>
            <h1 id="total">$ 0,00</h1>
          </fieldset>
          <a href="#" id="findeventa-link" class="button postfix">Finalizar Venta</a>
          <input id="checkbox1" type="checkbox"><label for="checkbox1">Cuenta Corriente</label>
        </div>
    </div>

    <div id="findeventa" class="reveal-modal" data-reveal>
      <div class="row">
        <div class="large-12 columns">
          <h2>Total: <span id="total_findeventa"></span></h2>
        </div>
      </div>
      <div class="row">
        <div class="large-3 columns pagaconrow1">
          <h2>Paga con:</h2>
        </div>
        <div class="large-9 columns pagaconrow2">
          <div class="pagaconsign">$</div>
          <input type="text" value="" id="pagacon" />
        </div>
      </div>
      <div class="row">
        <div class="large-12 columns">
          <h2>Vuelto: $ <span id="vuelto">-</span></h2>
        </div>
      </div>
      <div class="row">
        <div class="large-4 columns">
          <a href="#" class="button postfix" id="aceptarventa">Aceptar</a>
        </div>
      </div>
      <a class="close-reveal-modal">Volver a la venta</a>
    </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/jquery.ui.core.js"></script>
    <script src="js/vendor/jquery.ui.widget.js"></script>
    <script src="js/vendor/jquery.ui.position.js"></script>
    <script src="js/vendor/jquery.ui.menu.js"></script>
    <script src="js/vendor/jquery.ui.autocomplete.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/venta.js"></script>

    <?php
    echo "<script>";
    echo "var availableTags = [";
    $firstime = true;
    while ($row = mysql_fetch_assoc($producto)) {
      if ($firstime){
        echo '{ id:"' . $row['id'] . '", label: "' . $row['nombre'] . '", value: "' . $row['nombre'] . '", precio: "' . $row['precio'] . '", nombre: "' . $row['nombre'] . '", bc: "' . $row['codigo_de_barra'] . '" }';
        $firstime = false;
      }else{
          echo ',{ id:"' . $row['id'] . '", label: "' . $row['nombre'] . '", value: "' . $row['nombre'] . '", precio: "' . $row['precio'] . '", nombre: "' . $row['nombre'] . '", bc: "' . $row['codigo_de_barra'] . '" }';
      }
    }
    echo "];";
    echo "</script>";
    ?>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>

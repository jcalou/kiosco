<?php
require 'classes/clsConnectMySql.php';
require 'classes/clsProducto.php';
require 'classes/clsProveedor.php';
$page="productos";

$db = new DB();
$productos = new Productos($db);
$proveedores = new Proveedores($db);

if(isset($_GET['id'])){
  $producto = mysql_fetch_array($productos->getId($_GET['id']));
  $tipo = "editar";
}else{
  $producto['id_proveedor'] = "0";
  $tipo = "agregar";
}

$proveedor = $proveedores->getAll('_all',100,0);

?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Administracion - Detalle de producto</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/style.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    <?php include('includes/topmenu.php'); ?>

    <div class=" content row">
      <div class="large-12 columns">
        <form action="procesar-producto.php" method="POST" id="theform">
          <?php  if($tipo == "editar") { ?>
          <input type="hidden" name="id" value='<?php echo $producto['id']; ?>'>
          <?php }else{ ?>
          <input type="hidden" name="id" value="0">
          <?php } ?>
          <div class="row">
            <div class="large-6 columns">
              <label>Nombre
                <input type="text" name="nombre" value='<?=($tipo == "editar")?$producto['nombre']:""?>' required />
              </label>
            </div>
            <div class="large-6 columns">
              <label>Tipo
                <input type="text" name="tipo" value='<?=($tipo == "editar")?$producto['tipo']:""?>' />
              </label>
            </div>
          </div>
          <div class="row">
            <div class="large-6 columns">
              <label>Marca
                <input type="text" name="marca" value='<?=($tipo == "editar")?$producto['marca']:""?>' />
              </label>
            </div>
            <div class="large-6 columns">
              <label>Codigo de Barra
                <input type="text" name="codigo_de_barra" id="codigo_de_barra" value='<?=($tipo == "editar")?$producto['codigo_de_barra']:""?>' />
              </label>
            </div>
          </div>
          <div class="row">
            <div class="large-9 columns">
              <label>Proveedor
                <select name="id_proveedor">
                  <option value="0">Seleccione el proveedor</option>
                  <?php while ($row = mysql_fetch_assoc($proveedor)) { ?>
                    <option value="<?=$row['id']?>" <?=($row['id'] == $producto['id_proveedor'])?"selected='selected'":""?>><?=$row['nombre']?></option>
                  <?php } ?>
                </select>
              </label>
            </div>
            <div class="large-3 columns">
              <label>¿No existe?
                <a href="proveedor.php" class="button agregarproveedor">Agregar Proveedor</a>
              </label>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <label>Descripcion
                <textarea name="descripcion" id="descripcion"><?=($tipo == "editar")?$producto['descripcion']:""?></textarea>
              </label>
            </div>
          </div>
          <div class="row">
            <div class="large-6 columns">
              <label>Stock
                <input type="number" name="stock" value='<?=($tipo == "editar")?$producto['stock']:"1"?>' required />
              </label>
            </div>
            <div class="large-6 columns">
              <label>Stock minimo
                <input type="number" name="stock_minimo" value='<?=($tipo == "editar")?$producto['stock_minimo']:"1"?>' required />
              </label>
            </div>
          </div>
          <div class="row collapse">
            <div class="small-3 large-1 columns">
              <label>Costo
                <span class="prefix">$</span>
              </label>
            </div>
            <div class="small-9 large-2 columns">
              <label>&nbsp;
                <input type="text" name="costo" value='<?=($tipo == "editar")?$producto['costo']:""?>' id="costo" />
              </label>
            </div>
            <div class="large-1 columns">
              <label>&nbsp;
              </label>
            </div>
            <div class="small-3 large-1 columns">
              <label>Margen
                <span class="prefix">%</span>
              </label>
            </div>
            <div class="small-9 large-2 columns">
              <label>&nbsp;
                <input type="text" name="margen" value='<?=($tipo == "editar")?$producto['margen']:""?>' id="margen" />
              </label>
            </div>
            <div class="large-1 columns">
              <label>&nbsp;
              </label>
            </div>
            <div class="small-3 large-1 columns">
              <label>Precio
                <span class="prefix">$</span>
              </label>
            </div>
            <div class="small-9 large-2 columns">
              <label>&nbsp;
                <input type="text" name="precio" value='<?=($tipo == "editar")?$producto['precio']:""?>' required id="precio" />
              </label>
            </div>
            <div class="large-1 columns">
              <label>&nbsp;
              </label>
            </div>
          </div>
          <div class="row">
            <div class="large-4 columns">
              <input type="submit" value='Guardar' class="button success" />
              <a href="javascript:history.back();" class="button secondary">Cancelar</a>
            </div>
          </div>

        </form>
      </div>
    </div>


    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
        function updatePrecioSugerido() {
          var costo = Number($('#costo').val());
          var margen = Number($('#margen').val());

          var newnumber = (costo + (costo*margen)/100).toFixed(2);

          $('#precio').val(newnumber);

        }

        $(document).ready(function(){
          $('#margen').blur(function(e) {
            e.preventDefault();
            updatePrecioSugerido();
          });
          $('#costo').blur(function(e) {
            e.preventDefault();
            updatePrecioSugerido();
          });
          $('#theform').bind("keyup keypress", function(e) {
            var code = e.keyCode || e.which;
            if (code  == 13) {
              e.preventDefault();
              return false;
            }
          });
        });

    </script>

  </body>
</html>

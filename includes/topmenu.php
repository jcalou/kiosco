  <?php include('includes/caja.php'); ?>
  <div class="topmenu">
    <div class="row">
      <nav class="top-bar" data-topbar>
        <ul class="title-area">
          <li class="name">
            <h1><a href="index.php">Administraci&oacute;n</a></h1>
          </li>
        </ul>
        <section class="top-bar-section">
          <ul class="left">
            <li <?php if($page=="home") { echo "class='active'"; }; ?>><a href="index.php">Inicio</a></li>
            <li <?php if($page=="venta") { echo "class='active'"; }; ?>><a href="venta.php">Venta</a></li>
            <li <?php if($page=="egreso") { echo "class='active'"; }; ?>><a href="egreso.php">Egreso</a></li>
            <li class="has-dropdown <?php if($page=="productos") { echo "active"; }; ?>">
              <a href="listar-productos.php">Productos</a>
              <ul class="dropdown">
                <li><a href="listar-productos.php">Listar</a></li>
                <li><a href="producto.php">Agregar</a></li>
                <li><a href="actualizar-por-marca.php">Actualizar precios por marca</a></li>
              </ul>
            </li>
            <li class="has-dropdown <?php if($page=="proveedores") { echo "active"; }; ?>">
              <a href="listar-proveedores.php">Proveedores</a>
              <ul class="dropdown">
                <li><a href="listar-proveedores.php">Listar</a></li>
                <li><a href="proveedor.php">Agregar</a></li>
              </ul>
            </li>
            <li class="has-dropdown <?php if($page=="reportes") { echo "active"; }; ?>">
              <a href="reportes.php">Reportes</a>
              <ul class="dropdown">
                <li><a href="listar-ventas.php">Listar Ventas</a></li>
              </ul>
            </li>
          </ul>
          <ul class="right">
            <li><p>Fecha: <?=date("d-m-Y");?></p></li>
            <li><a href="caja.php" class="caja">Caja: $ <?php echo $cajavalue['caja'] ?></a></li>
          </ul>
        </section>
      </nav>
    </div>
  </div>

<?php
require 'classes/clsConnectMySql.php';
require 'classes/clsProducto.php';

$db = new DB();
$productos = new Productos($db);

$marca = $_POST['marca'];
$subir = 1 + ($_POST['subir']/100);

$prodMarca = $productos->getProductosMarca($marca);

while($row = mysql_fetch_assoc($prodMarca)) {

  if (is_null($row['costo']) || $row['costo']=="" || $row['costo']=="0"){
    $nuevocosto = $row['precio'] * $subir;
  } else {
    $nuevocosto = $row['costo'] * $subir;
  }  
  
  if (is_null($row['margen']) || $row['margen']=="" || $row['margen']=="0"){
    $nuevoprecio = $row['precio'] * $subir;
  } else {
    $nuevoprecio = $nuevocosto * (1 + ($row['margen']/100));
  }  

  $productos->nombre = $row['nombre'];
  $productos->tipo = $row['tipo'];
  $productos->id_proveedor = $row['id_proveedor'];
  $productos->codigo_de_barra = $row['codigo_de_barra'];
  $productos->marca = $row['marca'];
  $productos->descripcion = $row['descripcion'];
  $productos->stock = $row['stock'];
  $productos->stock_minimo = $row['stock_minimo'];
  $productos->costo = $nuevocosto;
  $productos->margen = $row['margen'];
  $productos->precio = $nuevoprecio;

  $productos->actualizar($row['id']);

}

header("Location: actualizar-por-marca.php");
die();
?>

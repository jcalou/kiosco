<?php
require 'classes/clsConnectMySql.php';
require 'classes/clsProducto.php';

$page="productos";

$db = new DB();
$productos = new Productos($db);

$marcas = $productos->getMarcas();

?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Administracion - Actualizar precios por marca</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/style.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    <?php include('includes/topmenu.php'); ?>

    <div class=" content row">
      <div class="large-12 columns">
        <div class="row">
          <div class="large-6 columns">
            <h3>Actualizar precios por marca</h3>
          </div>
        </div>
        <form action="procesar-por-marca.php" method="POST">
          <div class="row">
            <div class="large-4 columns">
              <label>Marca
                <select name="marca" id="marca">
                  <option value="0">Elegir Marca</option>
                  <?php while($row = mysql_fetch_assoc($marcas)) { ?>
                    <option value="<?=$row['marca'] ?>"><?=$row['marca'] ?> (<?=$row['c'] ?>)</option>
                  <?php } ?>
                </select>
              </label>
            </div>
            <div class="large-5 columns">
              <label>% a subir (sube el costo y calcula el nuevo precio)
                <input type="text" name="subir" placeholder="De 0 a 100 - S&oacute;lo n&uacute;meros" value='' required />
              </label>
            </div>
            <div class="large-3 columns">
              <label>&nbsp;
                <input type="submit" value="Aceptar" class="button postfix" />
              </label>
            </div>

          </div>

        </form>
      </div>
    </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>

    <script>
      $(document).foundation();
    </script>
  </body>
</html>

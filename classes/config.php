<?php
// WEBSITE INFO
DEFINE ('WEBSITE_URL', 'http://localhost/kiosco/'); // Database name.
DEFINE ('WEBSITE_MAIN', 'index.php'); // Website main page.

// MySQL
DEFINE ('DB_NAME', 'gestion'); // Database name.
DEFINE ('DB_USER', 'root'); // Database user.
DEFINE ('DB_PASS', ''); // Database password.
DEFINE ('DB_HOST', 'localhost'); // Database host.

// PAGINACION
DEFINE ('PAG_IPP', '10'); // Items per page default

// STOCK
DEFINE ('STOCK_DIF', '5'); // Diferencia minima de stock

?>

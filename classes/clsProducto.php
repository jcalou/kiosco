<?php

/**
* Producto
*/
class Productos {

  private $db;
  public $id;
  public $nombre;
  public $tipo;
  public $id_provedoor;
  public $codigo_de_barra;
  public $marca;
  public $descripcion;
  public $stock;
  public $stock_minimo;
  public $costo;
  public $margen;
  public $precio;
  public $fecha_carga;
  public $ultima_edicion;

  function __construct($db) {
    $this->db = $db;
  }

  function getAllCount() {
    $query = "SELECT count(*) FROM producto";
    $q = mysql_query($query);
    $count = mysql_fetch_row($q);
    return $count[0];
  }

  function getAll($q, $limit, $offset) {
    if ($q == "_all") {
      $query = "SELECT p.*, e.nombre as e_nombre, e.id as e_id FROM producto p LEFT JOIN proveedor e ON p.id_proveedor = e.id LIMIT $limit OFFSET $offset";
    }else{
      $query = "SELECT p.*, e.nombre as e_nombre, e.id as e_id FROM producto p LEFT JOIN proveedor e ON p.id_proveedor = e.id WHERE p.nombre LIKE '%".$q."%' OR p.codigo_de_barra LIKE '%".$q."%'";
    }
    $ret = mysql_query($query);
    return $ret;
  }

  function getId($id) {
    $query = "SELECT * FROM producto WHERE id = $id";
    $ret = mysql_query($query);
    return $ret;
  }

  function agregar() {
    $query = "INSERT INTO producto
      (nombre, tipo, id_proveedor, codigo_de_barra, marca, descripcion, stock, stock_minimo, costo, margen, precio, fecha_carga, ultima_edicion)
      VALUES
      ('$this->nombre', '$this->tipo', $this->id_proveedor, '$this->codigo_de_barra', '$this->marca', '$this->descripcion', $this->stock, $this->stock_minimo, '$this->costo', '$this->margen', '$this->precio', now(), now())";
    $ret = mysql_query($query);
    return $ret;
  }

  function actualizar($id) {
    $query = "UPDATE producto SET
      nombre = '$this->nombre',
      tipo = '$this->tipo',
      id_proveedor = $this->id_proveedor,
      codigo_de_barra = '$this->codigo_de_barra',
      marca = '$this->marca',
      descripcion = '$this->descripcion',
      stock = $this->stock,
      stock_minimo = $this->stock_minimo,
      costo = '$this->costo',
      margen = '$this->margen',
      precio = '$this->precio',
      ultima_edicion = now()
      WHERE id = $id";
    $ret = mysql_query($query);
    return $ret;
  }

  function actualizarStock($id,$stock) {
    $query = "UPDATE producto SET
      stock = $stock
      WHERE id = $id";
    $ret = mysql_query($query);
    return $ret;
  }

  function eliminar($id) {
    $query = "DELETE FROM producto WHERE id = $id";
    $ret = mysql_query($query);
    return $ret;
  }

  function getMarcas() {
    $query = "SELECT DISTINCT marca, count(id) as c FROM producto WHERE marca <> '' GROUP BY marca ORDER BY marca";
    $ret = mysql_query($query);
    return $ret;
  }

  function getTipos() {
    $query = "SELECT DISTINCT tipo, count(id) as c FROM producto WHERE tipo <> '' GROUP BY tipo ORDER BY tipo";
    $ret = mysql_query($query);
    return $ret;
  }

  function getProductosMarca($marca) {
    $query = "SELECT * FROM producto WHERE marca = '$marca'";
    $ret = mysql_query($query);
    return $ret;
  }

}

?>

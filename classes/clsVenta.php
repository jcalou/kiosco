<?php

/**
* Venta
*/
class Ventas {

  private $db;
  public $id;
  public $observaciones;
  public $total;
  public $pago_con;
  public $vuelto;
  public $cuenta_corriente;
  public $fecha_carga;

  public $id_producto;
  public $cantidad;
  public $precio_unitario;

  function __construct($db) {
    $this->db = $db;
  }

  function agregar() {
    $query = "INSERT INTO venta
      (observaciones,total,pago_con,vuelto,cuenta_corriente,fecha_carga)
      VALUES
      ('$this->observaciones', '$this->total', $this->pago_con, '$this->vuelto', '$this->cuenta_corriente', now())";
    mysql_query($query);
    $ret = mysql_insert_id();
    return $ret;
  }

  function agregar_detalle($id_venta) {
    $query = "INSERT INTO venta_detalle
      (id_venta,id_producto,cantidad,precio_unitario)
      VALUES
      ($id_venta, $this->id_producto, $this->cantidad, '$this->precio_unitario')";
    $ret = mysql_query($query);
    return $ret;
  }

  function getAllEgresos($limit, $offset) {
    $query = "SELECT * from venta WHERE total < 0 ORDER by fecha_carga DESC LIMIT $limit OFFSET $offset";
    $ret = mysql_query($query);
    return $ret;
  }

  function getAllEgresosCount() {
    $query = "SELECT count(*) FROM venta WHERE total < 0";
    $q = mysql_query($query);
    $count = mysql_fetch_row($q);
    return $count[0];
  }

  function agregarEgreso() {
    $query = "INSERT INTO venta
      (observaciones,total,fecha_carga)
      VALUES
      ('$this->observaciones', '$this->total', now())";
    mysql_query($query);
    $ret = mysql_insert_id();
    return $ret;
  }

  function eliminarEgreso($id) {
    $query = "DELETE FROM venta WHERE id = $id";
    $ret = mysql_query($query);
    return $ret;
  }

  function getAllCount() {
    $query = "SELECT count(*) FROM venta";
    $q = mysql_query($query);
    $count = mysql_fetch_row($q);
    return $count[0];
  }

  function getAll($q, $limit, $offset) {
    if ($q == "_all") {
      $query = "SELECT * FROM venta ORDER BY id desc LIMIT $limit OFFSET $offset";
    }else{
      $query = "SELECT * FROM venta WHERE fecha_carga LIKE '%".$q."%' ORDER BY id desc";
    }
    $ret = mysql_query($query);
    return $ret;
  }

  function getId($id) {
    $query = "SELECT * FROM venta WHERE id = $id";
    $ret = mysql_query($query);
    return $ret;
  }

  function getIdDetalle($id_venta) {
    $query = "SELECT v.*,p.nombre as nombre_producto FROM venta_detalle v LEFT JOIN producto p ON v.id_producto = p.id WHERE id_venta = $id_venta";
    $ret = mysql_query($query);
    return $ret;
  }


}

?>

<?php
require 'classes/clsConnectMySql.php';
require 'classes/clsProveedor.php';

$db = new DB();
$proveedores = new Proveedores($db);

if (isset($_GET['e']) && ($_GET['e'] == "1")) {
  //eliminar
  $id = $_GET['id'];
  $proveedores->eliminar($id);
  header("Location: listar-proveedores.php");
  die();
}

$proveedores->nombre = $_POST['nombre'];
$proveedores->direccion = $_POST['direccion'];
$proveedores->telefono = $_POST['telefono'];
$proveedores->contacto = $_POST['contacto'];
$proveedores->email = $_POST['email'];
$proveedores->cuit = $_POST['cuit'];
$proveedores->observaciones = $_POST['observaciones'];

if ($_POST['id']=="0") {
  // agregar
  $proveedores->agregar();
}else {
  // editar
  $proveedores->actualizar($_POST['id']);
};

header("Location: listar-proveedores.php");
die();
?>

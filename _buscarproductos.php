<?php

// no term passed - just exit early with no response
if (empty($_GET['term'])) die();

$q = strtolower($_GET["term"]);
// remove slashes if they were magically added
if (get_magic_quotes_gpc()) $q = stripslashes($q);

require 'classes/clsConnectMySql.php';
require 'classes/clsProducto.php';

$db = new DB();
$productos = new Productos($db);

$producto = $productos->getAll($_GET["term"],0,0);
$items = array();

while ($row = mysql_fetch_assoc($producto)) {
  $row_ = array();
  $row_['value'] = $row['codigo_de_barra'] . ' - ' . $row['nombre'] ;
  $row_['id'] = htmlentities(stripslashes($row['precio']));
  $items[] = $row_;
}

// json_encode is available in PHP 5.2 and above, or you can install a PECL module in earlier versions
echo json_encode($items);

?>

<?php
$page="egreso";

require 'classes/clsConnectMySql.php';
require 'classes/clsVenta.php';
require 'classes/clsPagination.php';

if(!isset($_GET['page'])){ $_GET['page'] = "1";}
if(!isset($_GET['ipp'])){ $_GET['ipp'] = PAG_IPP;}

$db = new DB();
$venta = new Ventas($db);

$count = $venta->getAllEgresosCount();

$pages = new Paginator();
$pages->items_total = $count;
$pages->mid_range = PAG_IPP;
$pages->paginate();

$limit = $pages->items_per_page;
$offset = ($pages->items_per_page * $pages->current_page) - $pages->items_per_page;

$result = $venta->getAllEgresos($limit, $offset);

?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Administracion - Egreso de dinero</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/themes/base/jquery.ui.all.css">
    <link rel="stylesheet" href="css/style.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    <?php include('includes/topmenu.php'); ?>

    <div class="content row">
      <div class="large-12 columns">
        <h4>Egreso de dinero</h4>
      </div>
      <form action="procesar-egreso.php" method="POST">
        <div class="large-6 columns">
          <div class="row collapse">
            <label>Descripcion
              <input type="text" placeholder="Descripcion" name="descripcion" id="descripcion" required />
            </label>
          </div>
        </div>
        <div class="large-4 columns">
          <div class="row collapse">
            <label>Monto
              <input type="text" placeholder="Monto en $" name="monto" id="monto" required />
            </label>
          </div>
        </div>

        <input type="hidden" id="id_hidden" value="0" />

        <div class="large-2 columns">
          <label>&nbsp;
            <input type="submit" value="Agregar" class="button postfix" />
          </label>
        </div>

      </form>
    </div>

    <div class=" content row">
      <div class="large-12 columns">
        <h4>Listado de &uacute;ltimos egresos</h4>
      </div>
      <div class="large-12 columns">
        <table width="100%">
          <thead>
            <tr>
              <th>Fecha</th>
              <th>Descripcion</th>
              <th>Monto</th>
              <!--th>&nbsp;</th-->
            </tr>
          </thead>
          <tbody>
          <?php while($row = mysql_fetch_assoc($result)) { ?>
            <tr>
              <td><?=date("d/m/Y",strtotime($row['fecha_carga'])) ?></td>
              <td><?=$row['observaciones'] ?></td>
              <td>$ <?=$row['total']*(-1) ?></td>
              <!--td><a href="procesar-egreso.php?e=1&id=<?=$row['id'] ?>" onclick = "if (! confirm('¿Est&aacute; seguro que desea eliminar este egreso?')) { return false; }"><i class="foundicon-remove"></i></a></td-->
            </tr>
          <?php }; ?>
          </tbody>
        </table>
      </div>
      <div class="large-12 columns pagination-centered pagination">
        <?php
          echo "P&aacute;gina $pages->current_page de $pages->num_pages<br/>"; 
          echo $pages->display_pages();
        ?>
      </div>

    </div>
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>

    <script>
      $(document).foundation();
    </script>
  </body>
</html>

<?php
require 'classes/clsConnectMySql.php';
require 'classes/clsVenta.php';
require 'classes/clsPagination.php';
$page="reportes";
if(!isset($_GET['page'])){ $_GET['page'] = "1";}
if(!isset($_GET['ipp'])){ $_GET['ipp'] = PAG_IPP;}

$db = new DB();
$ventas = new Ventas($db);

if(isset($_GET['q'])){
  $result = $ventas->getAll($_GET['q'],0,0);
}else{
  $count = $ventas->getAllCount("_all");

  $pages = new Paginator();
  $pages->items_total = $count;
  $pages->mid_range = PAG_IPP;
  $pages->paginate();

  $limit = $pages->items_per_page;
  $offset = ($pages->items_per_page * $pages->current_page) - $pages->items_per_page;
  $result = $ventas->getAll("_all",$limit, $offset);
}

?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Administraci&oacute;n - Lista de Ventas</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link href="css/font-awesome.css" rel="stylesheet">
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    <?php include('includes/topmenu.php'); ?>

    <div class=" content row">
          <?php
            if(isset($_GET['q'])){
              ?>
          <div class="large-12 columns">
            <h4>Filtrando por fecha: "<?=$_GET['q'] ?>"</h4>
          </div>
              <?php
            } else { ?>
          <div class="large-3 columns">
            <label for="">
            Seleccione una fecha <input id="dp3" class="small-3 medium-3 large-3 columns" size="16" type="text" value="" data-date-format="dd-mm-yyyy" />
            </label>
          </div>
            <?php  } ?>
          <div class="large-12 columns">
            <table width="100%">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Fecha</th>
                  <th>Total</th>
                  <th>CC</th>
                  <th>&nbsp;</th>
                </tr>
              </thead>
              <tbody>
              <?php 
              $totaldeldia = 0;
              while($row = mysql_fetch_assoc($result)) { ?>
                <tr>
                  <td><?=$row['id'] ?></td>
                  <td><?=date("d-m-Y",strtotime($row['fecha_carga'])); ?></td>
                  <td>$ <?=$row['total'] ?></td>
                  <td><?php if($row['cuenta_corriente']=="S"){echo '<i class="fa fa-check"></i>';} ?></td>
                  <td><a href="detalle-venta.php?id=<?=$row['id'] ?>"><i class="foundicon-edit"></i></a></td>
                </tr>
              <?php 
              $totaldeldia = $totaldeldia + $row['total'];
              }; ?>
              </tbody>
            </table>
          </div>
          <div class="large-12 columns pagination-centered pagination">
            <?php
            if(isset($_GET['q'])){
              ?>
              <h4>Total del dia: $<?=$totaldeldia?></h4>
              <a href="listar-ventas.php" class="button radius">Volver</a><?php
            }elseif($_GET['ipp'] != "All"){
              echo "P&aacute;gina $pages->current_page de $pages->num_pages<br/>"; echo $pages->display_pages();
            } ?>
          </div>
        </div>


    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/foundation/foundation.datepicker.js"></script>
    <script>
      $(document).foundation();

      function pad(num) {
        num = num + '';
        return num.length < 2 ? '0' + num : num;
      }

      function yyyymmdd(dateIn) {

        var yyyy = dateIn.getFullYear();
        var mm = dateIn.getMonth()+1; // getMonth() is zero-based
        var dd  = dateIn.getDate();
        return yyyy + "-" + pad(mm) + "-" + pad(dd);
      }

      $(document).ready(function(){

        $('#dp3').fdatepicker()
          .on('changeDate', function (ev) {
            var date_redirect = new Date(ev.date.valueOf() + (3 * 60 * 61000));
            window.location = "listar-ventas.php?q=" + yyyymmdd(date_redirect);

          });

      });
    </script>

  </body>
</html>

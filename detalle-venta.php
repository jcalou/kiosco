<?php $page="reportes";
require 'classes/clsConnectMySql.php';
require 'classes/clsVenta.php';

$db = new DB();
$ventas = new Ventas($db);

$venta = mysql_fetch_array($ventas->getId($_GET['id']));
$detalle = $ventas->getIdDetalle($_GET['id']);

?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Administracion</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link href="css/font-awesome.css" rel="stylesheet">
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    <?php include('includes/topmenu.php'); ?>

    <div class="content row">
      <div class="large-4 columns">
        Id Factura: <span style="font-size:30px;"><?=$venta['id'] ?></span>
      </div>
      <div class="large-4 columns">
        Fecha: <span style="font-size:30px;"><?=date("d-m-Y",strtotime($venta['fecha_carga'])); ?></span>
      </div>
      <div class="large-4 columns">
        Cuenta Corriente: <span style="font-size:30px;"><?php if($venta['cuenta_corriente']=="S"){echo '<i class="fa fa-check"></i>';}else{echo '<i class="fa fa-times"></i>';} ?></span>
      </div>
    </div>

    <div class="content row">
        <div class="large-8 columns">
          <table width="100%" id="items">
            <thead>
              <tr>
                <th>Producto</th>
                <th width="100">$ x U</th>
                <th width="55">Cant.</th>
                <th width="100">$</th>
              </tr>
            </thead>
            <tbody>
            <?php while($row = mysql_fetch_assoc($detalle)) { ?>
              <tr>
                <td><?=$row['nombre_producto'] ?></td>
                <td><?=$row['precio_unitario']; ?></td>
                <td><?=$row['cantidad'] ?></td>
                <td>$ <?=$row['cantidad']*strval(substr($row['precio_unitario'],1,14)) ?></td>
              </tr>
            <?php }; ?>
            </tbody>
          </table>
          <label>Observaciones / Comentarios
            <textarea name="observaciones" id="observaciones"><?=$venta['observaciones'] ?></textarea>
          </label>
        </div>
        <div class="large-4 columns">
          <fieldset class="total">
            <legend>TOTAL</legend>
            <h1 id="total">$ <?=$venta['total'] ?></h1>
          </fieldset>
        </div>
    </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>

    <script>
      $(document).foundation();
    </script>
  </body>
</html>

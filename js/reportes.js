$(document).ready(function(){

  $('#calculartotal').click(function(e) {
    e.preventDefault();

    $.ajax({
      type: "POST",
      url: "procesar-reporte-diario.php",
      data: {
        startDate: $('#startDate').text(),
        endDate: $('#endDate').text(),
        tipo: $('#tipo').val()
      }
    })
    .done(function(msg) {
      $('.totalreport').html(msg);
    })
    .fail(function() {
      //console.log("error");
    })
    .always(function() {
      //console.log("complete");
    });
  });

});

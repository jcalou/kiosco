<?php
require 'classes/clsConnectMySql.php';
require 'classes/clsProveedor.php';
require 'classes/clsPagination.php';
$page="proveedores";
if(!isset($_GET['page'])){ $_GET['page'] = "1";}
if(!isset($_GET['ipp'])){ $_GET['ipp'] = PAG_IPP;}

$db = new DB();
$proveedor = new Proveedores($db);

if(isset($_GET['q'])){
  $result = $proveedor->getAll($_GET['q'],0,0);
}else{
  $count = $proveedor->getAllCount("_all");

  $pages = new Paginator();
  $pages->items_total = $count;
  $pages->mid_range = PAG_IPP;
  $pages->paginate();

  $limit = $pages->items_per_page;
  $offset = ($pages->items_per_page * $pages->current_page) - $pages->items_per_page;
  $result = $proveedor->getAll("_all",$limit, $offset);
}

?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Administraci&oacute;n - Lista de Proveedores</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/style.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    <?php include('includes/topmenu.php'); ?>

    <div class="content row">
      <form action="listar-proveedores.php" method="GET">
        <div class="large-8 columns">
          <div class="row collapse">
            <div class="large-10 columns">
              <input type="text" placeholder="Nombre del proveedor" name="q" id="q" required />
            </div>
            <div class="large-2 columns">
              <input type="submit" value="Buscar" class="button postfix" />
            </div>
          </div>
        </div>
        <div class="large-4 columns">
          <a href="proveedor.php" class="button postfix">Agregar nuevo Proveedor</a>
        </div>
      </form>
    </div>

    <div class=" content row">
          <?php
            if(isset($_GET['q'])){
              ?>
          <div class="large-12 columns">
            <h4>Filtrando por "<?=$_GET['q'] ?>"</h4>
          </div>
              <?php
            } ?>
          <div class="large-12 columns">
            <table width="100%">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre</th>
                  <th>Direcci&oacute;n</th>
                  <th>Tel&eacute;fono</th>
                  <th>Contacto</th>
                  <th>E-mail</th>
                  <th>CUIT</th>
                  <th>&nbsp;</th>
                </tr>
              </thead>
              <tbody>
              <?php while($row = mysql_fetch_assoc($result)) { ?>
                <tr>
                  <td><?=$row['id'] ?></td>
                  <td><?=$row['nombre'] ?></td>
                  <td><?=$row['direccion'] ?></td>
                  <td><?=$row['telefono'] ?></td>
                  <td><?=$row['contacto'] ?></td>
                  <td><?=$row['email'] ?></td>
                  <td><?=$row['cuit'] ?></td>
                  <td><a href="proveedor.php?id=<?=$row['id'] ?>"><i class="foundicon-edit"></i></a>&nbsp;&nbsp;<a href="procesar-proveedor.php?e=1&id=<?=$row['id'] ?>" onclick = "if (! confirm('¿Est&aacute; seguro que desea eliminar este proveedor?')) { return false; }"><i class="foundicon-remove"></i></a></td>
                </tr>
              <?php }; ?>
              </tbody>
            </table>
          </div>
          <div class="large-12 columns pagination-centered pagination">
            <?php
            if(isset($_GET['q'])){
              ?><a href="listar-proveedores.php" class="button radius">Volver</a><?php
            }elseif($_GET['ipp'] != "All"){
              echo "P&aacute;gina $pages->current_page de $pages->num_pages<br/>"; echo $pages->display_pages();
            } ?>
          </div>
        </div>


    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>

  </body>
</html>
